var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');

gulp.task('serve', ['sass', 'js'], function(){
	browserSync.init({
		server: "./"
	});

	gulp.watch("styles/*.scss", ['sass']);
	gulp.watch("*.html").on('change', browserSync.reload)
	gulp.watch("js/*.js", ['js-watch']);
});

gulp.task('sass', function(){
	return gulp.src("styles/*.scss")
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(sourcemaps.init())
		.pipe(autoprefixer({
            browsers: ['last 2 versions', '> 1%'],
            cascade: false
        }))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest("dist/"))
		.pipe(browserSync.stream());
});

gulp.task('js-watch', ['js'], browserSync.reload);

gulp.task('js', function () {
    return gulp.src('js/*.js')
        //.pipe(browserify())
        .pipe(uglify())
        .pipe(concat("all.min.js"))
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);